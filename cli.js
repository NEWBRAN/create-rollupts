#!/usr/bin/env node
const argv = require('yargs').argv
const fsx = require('fs-extra')
const { join, resolve } = require('path')
// const debug = require('debug')('create-rollupts:cli')

const { main } = require('./index')


const pkg = 'package.json'
// should we normalize the arguments here
const baseDir = argv._.length ? resolve(argv._[0]) : process.cwd()
if (!fsx.existsSync(join(baseDir, pkg))) {
  console.error(`Could not locate ${pkg} in ${baseDir}!`)
  process.exit()
  return
}
const pkgJson = fsx.readJsonSync( join(baseDir, pkg) )

const skipInstall = !!argv.skipInstall || !!argv.s;

const yarn = !!argv.yarn || !!argv.y;

const help = !!argv.help || !!argv.h;

if (help === true) {
console.log(
`
Example:

  $ npx create-rollupts

The above command will create a Typescript development environment with Rollup and setup the ava test

Options

  1. Specify where to set up

  $ npx create-rollupts /path/to/where/you/want

  2. Don't run installation after setup complete

  $ npx create-rollupts --skipInstall

  or

  $ npx create-rollupts -s

  3. Use yarn instead of npm

  $ npx create-rollupts --yarn

  or

  $ npx create-rollupts -y


`
)

} else {

  main(baseDir, pkgJson, skipInstall, yarn)
    .then(() => {
      console.info(`Setup completed`)
    })
}
