'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var fsx = require('fs-extra');
var fsx__default = _interopDefault(fsx);
var _ = require('lodash');
var path = require('path');
var child_process = require('child_process');

// encap all the create rollup config methods in one
/*
import debug from 'debug'
const log = debug('create-rollupts:create-rollup-ts')
*/
// just embed the template instead of linking it
const tpl = `
import typescript from 'rollup-plugin-typescript2'
// if you need more fancy options then just add your own plugins
// and add to the plugins array
import pkg from './package.json'

export default {
  input: 'index.ts',
  output: [
    <%= output %>
  ],
  external: [
    ...Object.keys(pkg.dependencies || {}),
    ...Object.keys(pkg.peerDependencies || {})
  ],
  plugins: [
    typescript({
      typescript: require('typescript')
    })
  ]
}
`;
/**
 * @param {object} pkg package.json content make it any just can't be bother
 * @return {object} a constructed object for template, same as above
 */
function getFieldsForTpl(pkg) {
    const searchFields = ['main', 'module', 'browser'];
    return searchFields
        .filter((value, index, arr) => {
        return pkg[value] !== undefined;
    })
        .map((value, index, arr) => {
        let obj = {
            file: value // we will transform this again inside the template
        };
        switch (value) {
            case 'browser':
                obj.name = _.camelCase(pkg.name);
                obj.format = 'umd';
                break;
            case 'main':
                obj.format = 'cjs';
                break;
            case 'module':
                obj.format = 'es';
        }
        return { [value]: obj };
    })
        .reduce((last, next) => {
        return _.merge(last, next);
    }, {});
}
/**
 * String version of the object configuration
 * @param {object} config the output from the above method
 * @return {string} result
 */
function createConfigStr(config) {
    let output = [];
    _.forEach(config, (value) => {
        let str = `{
      file: pkg.${value.file},
      format: '${value.format}'
      ${value.name ? ", name: '" + value.name + "'" : ''}
    }`;
        output.push(str);
    });
    return output.join(',');
}
/**
 * @param {string} baseDir could change by user
 * @param {object} pkg the package.json object, use any just can't be bother
 * @return {object} promise
 */
function createRollupConfig(baseDir, pkg) {
    return new Promise((resolver, rejecter) => {
        // get the compiled method
        const compiled = _.template(tpl);
        // get the compiled template file
        const data = compiled({
            output: createConfigStr(getFieldsForTpl(pkg))
        });
        // output
        fsx.outputFile(path.join(baseDir, 'rollup.config.js'), data, err => {
            if (err) {
                console.error(`Failed to create rollup.config.js`, err);
                return rejecter(err);
            }
            return resolver(true);
        });
    });
}

// ava methods
function createAva(baseDir, pkg) {
    // setup some of the scripts cmd
    if (pkg.scripts === undefined) {
        pkg.scripts = {};
    }
    pkg.scripts = _.extend(pkg.scripts, {
        "test": "ava --verbose",
        "build": "rollup -c",
        "dev": "rollup -cw"
    });
    // insert an ava config into it
    if (pkg.ava === undefined) {
        pkg.ava = {
            "files": [
                "tests/*.test.js",
                "!tests/fixtures/*.*"
            ],
            "cache": true,
            "concurrency": 5,
            "failFast": true,
            "failWithoutAssertions": false,
            "tap": false,
            "compileEnhancements": false,
            "extensions": [
                "ts"
            ],
            "require": [
                "ts-node/register"
            ]
        };
        const file = path.join(baseDir, 'package.json');
        fsx.outputJsonSync(file, pkg, { spaces: 2 });
        // read it out again
        return fsx.readJsonSync(file);
    }
    // did nothing
    return false;
}

const tsConfig = `
{
  "compilerOptions": {
    /* Basic Options */
    // "incremental": true,                   /* Enable incremental compilation */
    "target": "ES5",                     /* Specify ECMAScript target version: 'ES3' (default), 'ES5', 'ES2015', 'ES2016', 'ES2017', 'ES2018', 'ES2019' or 'ESNEXT'. */
    "module": "es2015",                     /* Specify module code generation: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', or 'ESNext'. */
    // "lib": [],                             /* Specify library files to be included in the compilation. */
    // "allowJs": true,                       /* Allow javascript files to be compiled. */
    // "checkJs": true,                       /* Report errors in .js files. */
    // "jsx": "preserve",                     /* Specify JSX code generation: 'preserve', 'react-native', or 'react'. */
    // "declaration": true,                   /* Generates corresponding '.d.ts' file. */
    // "declarationMap": true,                /* Generates a sourcemap for each corresponding '.d.ts' file. */
    // "sourceMap": true,                     /* Generates corresponding '.map' file. */
    // "outFile": "./",                       /* Concatenate and emit output to single file. */
    "outDir": "./dist",                        /* Redirect output structure to the directory. */
    // "rootDir": "./",                       /* Specify the root directory of input files. Use to control the output directory structure with --outDir. */
    // "composite": true,                     /* Enable project compilation */
    // "tsBuildInfoFile": "./",               /* Specify file to store incremental compilation information */
    // "removeComments": true,                /* Do not emit comments to output. */
    // "noEmit": true,                        /* Do not emit outputs. */
    // "importHelpers": true,                 /* Import emit helpers from 'tslib'. */
    // "downlevelIteration": true,            /* Provide full support for iterables in 'for-of', spread, and destructuring when targeting 'ES5' or 'ES3'. */
    // "isolatedModules": true,               /* Transpile each file as a separate module (similar to 'ts.transpileModule'). */

    /* Strict Type-Checking Options */
    "strict": true,                           /* Enable all strict type-checking options. */
    // "noImplicitAny": true,                 /* Raise error on expressions and declarations with an implied 'any' type. */
    // "strictNullChecks": true,              /* Enable strict null checks. */
    // "strictFunctionTypes": true,           /* Enable strict checking of function types. */
    // "strictBindCallApply": true,           /* Enable strict 'bind', 'call', and 'apply' methods on functions. */
    // "strictPropertyInitialization": true,  /* Enable strict checking of property initialization in classes. */
    // "noImplicitThis": true,                /* Raise error on 'this' expressions with an implied 'any' type. */
    // "alwaysStrict": true,                  /* Parse in strict mode and emit "use strict" for each source file. */

    /* Additional Checks */
    // "noUnusedLocals": true,                /* Report errors on unused locals. */
    // "noUnusedParameters": true,            /* Report errors on unused parameters. */
    // "noImplicitReturns": true,             /* Report error when not all code paths in function return a value. */
    // "noFallthroughCasesInSwitch": true,    /* Report errors for fallthrough cases in switch statement. */

    /* Module Resolution Options */
    // "moduleResolution": "node",            /* Specify module resolution strategy: 'node' (Node.js) or 'classic' (TypeScript pre-1.6). */
    // "baseUrl": "./",                       /* Base directory to resolve non-absolute module names. */
    // "paths": {},                           /* A series of entries which re-map imports to lookup locations relative to the 'baseUrl'. */
    // "rootDirs": [],                        /* List of root folders whose combined content represents the structure of the project at runtime. */
    // "typeRoots": [],                       /* List of folders to include type definitions from. */
    // "types": [],                           /* Type declaration files to be included in compilation. */
    // "allowSyntheticDefaultImports": true,  /* Allow default imports from modules with no default export. This does not affect code emit, just typechecking. */
    "esModuleInterop": true                   /* Enables emit interoperability between CommonJS and ES Modules via creation of namespace objects for all imports. Implies 'allowSyntheticDefaultImports'. */
    // "preserveSymlinks": true,              /* Do not resolve the real path of symlinks. */
    // "allowUmdGlobalAccess": true,          /* Allow accessing UMD globals from modules. */

    /* Source Map Options */
    // "sourceRoot": "",                      /* Specify the location where debugger should locate TypeScript files instead of source locations. */
    // "mapRoot": "",                         /* Specify the location where debugger should locate map files instead of generated locations. */
    // "inlineSourceMap": true,               /* Emit a single file with source maps instead of having a separate file. */
    // "inlineSources": true,                 /* Emit the source alongside the sourcemaps within a single file; requires '--inlineSourceMap' or '--sourceMap' to be set. */

    /* Experimental Options */
    // "experimentalDecorators": true,        /* Enables experimental support for ES7 decorators. */
    // "emitDecoratorMetadata": true,         /* Enables experimental support for emitting type metadata for decorators. */
  }
}
`;
function createTsConfig(baseDir) {
    return new Promise((resolver, rejecter) => {
        fsx__default.outputFile(path.join(baseDir, 'tsconfig.json'), tsConfig, err => {
            if (err) {
                console.error(`Failed to create rollup.config.js`, err);
                return rejecter(err);
            }
            resolver();
        });
    });
}

// run installation unless user pass --skipInstall
const deps = ['ts-node', 'typescript', 'rollup', 'rollup-plugin-typescript2', 'ava'];
/**
 * @param {string} baseDir where to execute this call
 * @param {boolean} yarn use yarn or not
 * @return {object} promise
 */
function runInstall(baseDir, yarn) {
    return new Promise((resolver, rejecter) => {
        const ps = child_process.spawn(yarn ? 'yarn' : 'npm', [
            yarn ? 'add' : 'install',
            yarn ? '--dev' : '--save-dev'
        ].concat(deps), {
            cwd: baseDir,
            env: process.env
        });
        ps.stdout.on('data', (data) => {
            console.info('stdout', `${data}`);
        });
        ps.stderr.on('data', (data) => {
            console.error('stderr', `${data}`);
        });
        ps.on('close', (code) => {
            if (code !== 0) {
                return rejecter();
            }
            resolver();
        });
    });
}

// this tool is actually written in Typescript
// @TODO combine this into one interface for the
// cli to use also report each function for testing
/**
 * @param {string} baseDir where to execute this command
 * @param {object} pkg package.json content
 * @param {boolean} skipInstall skip installation
 * @param {boolean} yarn npm is default or use yarn
 * @return {object} promise
 */
function main(baseDir, pkg, skipInstall = false, yarn = false) {
    return Promise.all([
        createRollupConfig(baseDir, pkg),
        createTsConfig(baseDir)
    ])
        .then(() => {
        return createAva(baseDir, pkg);
    })
        .then(() => {
        if (!skipInstall) {
            console.info('Running installation');
            return runInstall(baseDir, yarn);
        }
        console.info('Installation skipped');
        return true;
    });
}

exports.createAva = createAva;
exports.createRollupConfig = createRollupConfig;
exports.createTsConfig = createTsConfig;
exports.main = main;
exports.runInstall = runInstall;
