import typescript from 'rollup-plugin-typescript2'
// import builtins from 'rollup-plugin-node-builtins'
// if you need more fancy options then just add your own plugins
// and add to the plugins array
import pkg from './package.json'

const external = [
  ...Object.keys(pkg.dependencies || {}),
  ...Object.keys(pkg.peerDependencies || {}),
  'path',
  'child_process'
]
// export 
export default {
  input: 'index.ts',
  output: [
    {
      file: pkg.main,
      format: 'cjs'
    }
  ],
  external: external,
  plugins: [
    typescript({
      typescript: require('typescript')
    })
  ]
}
