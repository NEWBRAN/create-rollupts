// run installation unless user pass --skipInstall
// import * as fsx from 'fs-extra'
// import * as _ from 'lodash'
// import { join } from 'path'
import { spawn } from 'child_process'

const deps = ['ts-node', 'typescript', 'rollup', 'rollup-plugin-typescript2', 'ava']

/**
 * @param {string} baseDir where to execute this call
 * @param {boolean} yarn use yarn or not
 * @return {object} promise
 */
export default function runInstall(baseDir: string, yarn: boolean): any {
  return new Promise((resolver: any, rejecter: any) => {
    const ps = spawn(
      yarn ? 'yarn' : 'npm',
      [
        yarn ? 'add' : 'install',
        yarn ? '--dev' : '--save-dev'
      ].concat(deps),
      {
        cwd: baseDir,
        env: process.env
      })

    ps.stdout.on('data', (data:any) => {
      console.info('stdout', `${data}`)
    })

    ps.stderr.on('data', (data:any) => {
      console.error('stderr', `${data}`)
    })

    ps.on('close', (code: number) => {
      if (code !== 0) {
        return rejecter()
      }
      resolver()
    })
  })
}
