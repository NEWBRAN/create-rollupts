// ava methods
import * as fsx from 'fs-extra'
import { join } from 'path'
import * as _ from 'lodash'

export default function createAva(baseDir: string, pkg: any): any {
  // setup some of the scripts cmd
  if (pkg.scripts === undefined) {
    pkg.scripts = {}
  }
  
  pkg.scripts = _.extend(pkg.scripts, {
    "test": "ava --verbose",
    "build": "rollup -c",
    "dev": "rollup -cw"
  })

  // insert an ava config into it
  if (pkg.ava === undefined) {
    pkg.ava = {
      "files": [
        "tests/*.test.js", // should that be js? ts never works ... but anyway
        "!tests/fixtures/*.*"
      ],
      "cache": true,
      "concurrency": 5,
      "failFast": true,
      "failWithoutAssertions": false,
      "tap": false,
      "compileEnhancements": false,
      "extensions": [
        "ts"
      ],
      "require": [
        "ts-node/register"
      ]
    }

    const file = join(baseDir, 'package.json')

    fsx.outputJsonSync( file, pkg , {spaces: 2})
    // read it out again
    return fsx.readJsonSync( file )
  }
  // did nothing
  return false;
}
