// encap all the create rollup config methods in one
import * as fsx from 'fs-extra'
import * as _ from 'lodash'
import { join } from 'path'
/*
import debug from 'debug'
const log = debug('create-rollupts:create-rollup-ts')
*/
// just embed the template instead of linking it
const tpl = `
import typescript from 'rollup-plugin-typescript2'
// if you need more fancy options then just add your own plugins
// and add to the plugins array
import pkg from './package.json'

export default {
  input: 'index.ts',
  output: [
    <%= output %>
  ],
  external: [
    ...Object.keys(pkg.dependencies || {}),
    ...Object.keys(pkg.peerDependencies || {})
  ],
  plugins: [
    typescript({
      typescript: require('typescript')
    })
  ]
}
`;

// interface(s)
interface outputConfigType {
  name?: string,
  format?: string, // this is really shit
  file: string
}

/**
 * @param {object} pkg package.json content make it any just can't be bother
 * @return {object} a constructed object for template, same as above
 */
function getFieldsForTpl(pkg: any): any {
  const searchFields = ['main', 'module', 'browser']
  return searchFields
    .filter((value: string, index: number, arr: any[]) => {
      return pkg[value] !== undefined;
    })
    .map((value: string, index: number, arr: any[]) => {
      let obj: outputConfigType = {
        file: value // we will transform this again inside the template
      }
      switch (value) {
        case 'browser':
          obj.name = _.camelCase(pkg.name)
          obj.format = 'umd'
          break;
        case 'main':
          obj.format = 'cjs'
          break;
        case 'module':
          obj.format = 'es'
      }
      return {[value]: obj};
    })
    .reduce((last: object, next: object): object => {
      return _.merge(last, next)
    }, {})
}

/**
 * String version of the object configuration
 * @param {object} config the output from the above method
 * @return {string} result
 */
function createConfigStr(config: any): string {
  let output: string[] = [];
  _.forEach(config, (value: any) => {
    let str = `{
      file: pkg.${value.file},
      format: '${value.format}'
      ${value.name ? ", name: '" + value.name + "'" : ''}
    }`
    output.push(str)
  })
  return output.join(',')
}

/**
 * @param {string} baseDir could change by user
 * @param {object} pkg the package.json object, use any just can't be bother
 * @return {object} promise
 */
export default function createRollupConfig(baseDir: string, pkg: any): any {
  return new Promise((resolver, rejecter) => {
    // get the compiled method
    const compiled = _.template(tpl)
    // get the compiled template file
    const data = compiled({
      output: createConfigStr(getFieldsForTpl(pkg))
    })
    // output
    fsx.outputFile(join(baseDir, 'rollup.config.js'), data, err => {
      if (err) {
        console.error(`Failed to create rollup.config.js`, err)
        return rejecter(err)
      }
      return resolver(true)
    })
  })
}
