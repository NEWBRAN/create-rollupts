# create-rollupts

#### Description

Run `npx create-rollupts` and you get a Typescript development environment with Rollup, plus testing with ava.

[中文 README](https://gitee.com/NEWBRAN/create-rollupts/blob/master/README.md)

#### Installation

CD into your project root then:

```sh
$ npx create-rollupts
```

#### Instructions

Specify where you want it to install



```sh
$ npx create-rollupts /path/to/your/project/root

```
---

Use yarn instead of npm

```sh
$ npx create-rollupts --yarn
```

or

```sh
$ npx create-rollupts -y
```

---

Skip installation at the end (**DON'T DO THIS UNLESS YOU KNOW WHAT YOU ARE DOING**)

```sh
$ npx create-rollupts --skipInstall
```

or

```sh
$ npx create-rollupts -s
```

---

Show help and not run anything

```sh
$ npx create-rollupts -h
```


#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

---

ISC

[Joel Chu](https://joelchu.com) (c) 2019
