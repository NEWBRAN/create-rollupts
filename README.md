# create-rollupts

#### 介绍

运行"npx create-rollupts",您将获得具有 Rollup 的 Typescript 开发环境,以及使用 ava 进行测试。

[English README](https://gitee.com/NEWBRAN/create-rollupts/blob/master/README.en.md)

#### 安装教程

到项目根目录,然后:

```sh
$ npx create-rollupts
```

#### 使用说明

指定要安装的位置

```sh
$ npx create-rollupts /path/to/your/project/root

```
---

使用 `yarn` 代替 `npm`

```sh
$ npx create-rollupts --yarn
```

or

```sh
$ npx create-rollupts -y
```

---

跳过最后安装(**如果你不知道你在做什么,那就不要做**)

```sh
$ npx create-rollupts --skipInstall
```

or

```sh
$ npx create-rollupts -s
```

---

显示帮助,不运行任何内容

```sh
$ npx create-rollupts -h
```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

---

ISC

[Joel Chu](https://joelchu.com) (c) 2019
