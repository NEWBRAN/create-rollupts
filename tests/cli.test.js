// test the combine cli interface
const test = require('ava')
const { join, resolve } = require('path')
const { spawn } = require('child_process')
const fsx = require('fs-extra')

const debug = require('debug')('create-rollupts:test:cli')

const baseDir = join(__dirname, 'fixtures', 'tmp2')
const rootDir = join(__dirname, '..')
const pkg = 'package.json'

const pathToCli = resolve(join(__dirname, '..', 'cli.js'))

test.before(async t => {
  const src = join(rootDir, pkg)
  const dest = join(baseDir, pkg)
  // prepare the tmp folder
  let packageJson = fsx.readJsonSync( src )

  delete packageJson.ava
  delete packageJson.scripts

  fsx.outputJsonSync( dest, packageJson, { spaces: 2 })
})

test.after(t => {
  fsx.removeSync( baseDir )
})


test.cb('It should able to run the cmd version and accept options', t => {

  t.plan(3)

  const es = spawn('node', [
    pathToCli,
    // '--',
    baseDir,
    '--skipInstall'
  ])

  es.stdout.on('data', data => {
    debug('stdout', `${data}`)
  })

  es.stderr.on('data', data => {
    debug('stderr', `${data}`)
  })

  es.on('close', code => {
    if (code !== 0) {
      t.fail()
    } else {

      t.truthy( fsx.existsSync( join(baseDir, pkg) ) )
      t.truthy( fsx.existsSync( join(baseDir, 'rollup.config.js') ) )
      t.truthy( fsx.existsSync( join(baseDir, 'tsconfig.json') ) )

      t.end()

    }
  })

})
