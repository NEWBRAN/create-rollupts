// try a typescript test file
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')

const debug = require('debug')('create-rollupts:test:basic')

const { createRollupConfig, createTsConfig, createAva } = require('../index')

const baseDir = join(__dirname, 'fixtures', 'tmp1')
const rootDir = join(__dirname, '..')
const pkg = 'package.json'
const ruFile = 'rollup.config.js'
const tsConfigJson = 'tsconfig.json'

test.before(async t => {
  const src = join(rootDir, pkg)
  const dest = join(baseDir, pkg)
  // prepare the tmp folder
  let packageJson = fsx.readJsonSync( src )

  delete packageJson.ava

  fsx.outputJsonSync( dest, packageJson, { spaces: 2 })
})

test.after(t => {
  fsx.removeSync( baseDir )
})

test('It should able to create a rollup.config.js in the project root folder', async t => {
  const file = join(baseDir, pkg)

  t.truthy( fsx.existsSync( file ) )

  const pkgJson = fsx.readJsonSync( file )

  const res = await createRollupConfig(baseDir, pkgJson)

  t.truthy( fsx.existsSync(baseDir, ruFile) )
})

test('It should create a tsconfig.json on the baseDir', async t => {

  await createTsConfig(baseDir)

  t.truthy( fsx.existsSync( join(baseDir, tsConfigJson) ))
})

test('It should able to update the package.json with ava configuration', async t => {

  const packageJson = fsx.readJsonSync( join(baseDir, pkg) )

  t.falsy( packageJson.ava )

  const result = createAva(baseDir, packageJson)

  t.truthy( result.ava )

})
