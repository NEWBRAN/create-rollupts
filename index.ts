
// this tool is actually written in Typescript
// import * as fsx from 'fs-extra'
// import { join, resolve } from 'path'
// it's a TS but TS thinks its ES WTF?
import {
  createAva,
  createRollupConfig,
  createTsConfig,
  runInstall
} from './src/index'

// @TODO combine this into one interface for the
// cli to use also report each function for testing
/**
 * @param {string} baseDir where to execute this command
 * @param {object} pkg package.json content
 * @param {boolean} skipInstall skip installation
 * @param {boolean} yarn npm is default or use yarn
 * @return {object} promise
 */
function main(baseDir: string, pkg: any, skipInstall: boolean = false, yarn: boolean = false): any {
  return Promise.all([
    createRollupConfig(baseDir, pkg),
    createTsConfig(baseDir)
  ])
  .then(() => {
    return createAva(baseDir, pkg)
  })
  .then(() => {
    if (!skipInstall) {
      console.info('Running installation')
      return runInstall(baseDir, yarn)
    }
    console.info('Installation skipped')
    return true;
  })
}

export {
  createAva,
  createRollupConfig,
  createTsConfig,
  runInstall,
  main
}
